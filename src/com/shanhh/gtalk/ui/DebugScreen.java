package com.shanhh.gtalk.ui;

import net.rim.device.api.i18n.DateFormat;
import net.rim.device.api.ui.component.RichTextField;
import net.rim.device.api.ui.container.MainScreen;

public class DebugScreen extends MainScreen {
	
	public static final int L_DEBUG 	=	9;
	public static final int L_WARN	=	5;
	public static final int L_ERROR	=	3;
	
	private static DateFormat dateGen = DateFormat.getInstance(DateFormat.TIME_DEFAULT);
	
	public void addDebugMsg(int level, final String msg) {
		long time = System.currentTimeMillis();
		String timeStr = dateGen.formatLocal(time);
		
		this.add(new RichTextField(timeStr+" : "+msg));
	}

}
